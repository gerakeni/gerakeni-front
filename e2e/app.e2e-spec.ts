import { GerakeniPage } from './app.po';

describe('gerakeni App', () => {
  let page: GerakeniPage;

  beforeEach(() => {
    page = new GerakeniPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
