import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ConnectionService} from "./connection-service";
/**
 * Created by Tianome on 24/03/2017.
 */

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private connectionService: ConnectionService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.connectionService.isUserConnected()){
      return true;
    }

    this.router.navigateByUrl("/connection").then((status) =>{
      console.log('status : ' + status);
    }).catch((exception) => {
      console.error('Error', exception);
    });
    return false;
  }
}
