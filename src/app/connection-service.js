"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var ConnectionService = (function () {
    function ConnectionService(http) {
        this.http = http;
        this.user = null;
    }
    ConnectionService.prototype.getData = function (dataUrl) {
        return this.http.get(dataUrl).map(function (res) { return res.json(); });
    };
    ConnectionService.prototype.postData = function (dataUrl, data) {
        return this.http.post(dataUrl, JSON.stringify(data)).map(function (res) { return res.json(); });
    };
    ConnectionService.prototype.getUser = function () {
        return this.user;
    };
    ConnectionService.prototype.setUser = function (user) {
        this.user = user;
    };
    ConnectionService.prototype.isUserConnected = function () {
        return this.user != null;
    };
    ConnectionService = __decorate([
        core_1.Injectable()
    ], ConnectionService);
    return ConnectionService;
}());
exports.ConnectionService = ConnectionService;
