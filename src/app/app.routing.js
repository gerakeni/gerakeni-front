/**
 * Created by Tianome on 01/03/2017.
 */
"use strict";
var connection_component_1 = require('./connection/connection.component');
var account_component_1 = require('./account/account.component');
var router_1 = require('@angular/router');
var home_component_1 = require('./home/home.component');
var auth_guard_1 = require("./auth.guard");
var music_component_1 = require("./music/music.component");
var movie_component_1 = require("./movie/movie.component");
var APP_ROUTES = [
    {
        path: '',
        redirectTo: '/connection',
        pathMatch: 'full'
    },
    {
        path: 'connection',
        component: connection_component_1.ConnectionComponent
    },
    {
        path: 'account',
        component: account_component_1.AccountComponent,
        canActivate: [auth_guard_1.AuthGuard]
    },
    {
        path: 'home',
        component: home_component_1.HomeComponent,
        canActivate: [auth_guard_1.AuthGuard]
    },
    {
        path: 'music',
        component: music_component_1.MusicComponent,
    },
    {
        path: 'movie',
        component: movie_component_1.MovieComponent,
    },
    {
        path: 'art',
        component: ArtComponent,
    }
];
exports.routing = router_1.RouterModule.forRoot(APP_ROUTES);
