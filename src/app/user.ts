export class User {
  private _username: string;
  private _password: string;
  private _email: string;

  constructor(data) {
    this.username = data.username;
    this.password = data.password;
    this.email = data.email;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }
}
