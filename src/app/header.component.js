"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var global_1 = require("./global");
var HeaderComponent = (function () {
    function HeaderComponent(connectionService, router) {
        this.connectionService = connectionService;
        this.router = router;
    }
    HeaderComponent.prototype.isUserConnected = function () {
        return this.connectionService.isUserConnected();
    };
    HeaderComponent.prototype.toggleShow = function () {
        global_1.Global.USER_SIGN = true;
    };
    HeaderComponent.prototype.toggleHide = function () {
        global_1.Global.USER_SIGN = false;
        console.log(global_1.Global.USER_SIGN);
    };
    HeaderComponent.prototype.logOut = function () {
        this.connectionService.setUser(null);
        this.router.navigateByUrl('/connection');
    };
    HeaderComponent.prototype.goTo = function () {
        this.router.navigateByUrl('/account');
    };
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styles: []
        })
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;
