"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
/**
 * Created by Tianome on 24/03/2017.
 */
var AuthGuard = (function () {
    function AuthGuard(router, connectionService) {
        this.router = router;
        this.connectionService = connectionService;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (this.connectionService.isUserConnected()) {
            return true;
        }
        this.router.navigateByUrl("/connection").then(function (status) {
            console.log('status : ' + status);
        }).catch(function (exception) {
            console.error('Error', exception);
        });
        return false;
    };
    AuthGuard = __decorate([
        core_1.Injectable()
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;
