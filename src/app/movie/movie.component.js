"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var MovieComponent = (function () {
    function MovieComponent(connectionService) {
        this.connectionService = connectionService;
        this.url = "http://localhost:8000/movie/search";
        this.response = {};
    }
    MovieComponent.prototype.ngOnInit = function () {
    };
    MovieComponent.prototype.searchMovie = function () {
        var _this = this;
        console.log('search');
        if (this.search != "") {
            this.connectionService.postData(this.url, { search: this.search }).subscribe(function (data) {
                _this.response = JSON.parse(data);
                console.log(_this.response);
            }, function (error) { return console.log(error); }, function () { return console.log('finished'); });
        }
        else {
            console.log('criteria empty');
        }
    };
    MovieComponent = __decorate([
        core_1.Component({
            selector: 'app-movie',
            templateUrl: './movie.component.html',
            styleUrls: ['./movie.component.css']
        })
    ], MovieComponent);
    return MovieComponent;
}());
exports.MovieComponent = MovieComponent;
