import { Component, OnInit } from '@angular/core';
import {ConnectionService} from "../connection-service";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  private search:string;
  private url = "http://localhost:8000/movie/search";
  public response = {};
  constructor(private connectionService : ConnectionService) { }

  ngOnInit() {
  }
  searchMovie(){
    console.log('search');
    if(this.search != ""){
      this.connectionService.postData(this.url,{search: this.search}).subscribe(
        data => {
          this.response = JSON.parse(data);
          console.log(this.response);
        },
        error => console.log(error),
        () => console.log('finished')
      );
    }
    else {
      console.log('criteria empty');
    }
  }
}
