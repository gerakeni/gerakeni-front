import {Component} from '@angular/core';
import {Global} from "./global";
import {ConnectionService} from "./connection-service";
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent {

  constructor(private connectionService : ConnectionService, private router:Router){
  }

  isUserConnected(){
    return this.connectionService.isUserConnected();
  }

  toggleShow() {
    Global.USER_SIGN = true;
  }

  toggleHide() {
    Global.USER_SIGN = false;
    console.log(Global.USER_SIGN);
  }
  logOut(){
    this.connectionService.setUser(null);
    this.router.navigateByUrl('/connection');
  }
  goTo(route){
    this.router.navigateByUrl(route);
  }
}
