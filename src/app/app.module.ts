import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AlertModule} from 'ng2-bootstrap';
import {AppComponent} from './app.component';
import {ConnectionComponent} from './connection/connection.component';
import {routing} from "./app.routing";
import { HeaderComponent } from './header.component';
import { Global } from './global';
import { ConnectionService } from './connection-service';
import { AccountComponent } from './account/account.component';
import { HomeComponent } from './home/home.component';
import {AuthGuard} from "./auth.guard";
import { MusicComponent } from './music/music.component';
import { ArtComponent } from './art/art.component';
import { MovieComponent } from './movie/movie.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ConnectionComponent,
    HeaderComponent,
    AccountComponent,
    HomeComponent,
    MusicComponent,
    ArtComponent,
    MovieComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule,
    routing,
    NgbModule.forRoot()
  ],
  providers: [Global,ConnectionService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
