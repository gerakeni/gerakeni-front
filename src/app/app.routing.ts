/**
 * Created by Tianome on 01/03/2017.
 */

import {ConnectionComponent} from './connection/connection.component';
import {AccountComponent}from './account/account.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from "./auth.guard";
import {MusicComponent} from "./music/music.component";
import {ArtComponent} from "./art/art.component";
import {MovieComponent} from "./movie/movie.component";

const APP_ROUTES: Routes = [
    {
      path: '',
      redirectTo: '/connection',
      pathMatch: 'full'
    },
    {
      path: 'connection',
      component: ConnectionComponent
    },
    {
      path: 'account',
      component: AccountComponent,
      canActivate: [AuthGuard]
    },
    {
      path: 'home',
      component: HomeComponent,
      canActivate: [AuthGuard]
    },
    {
      path: 'music',
      component: MusicComponent,
      // canActivate:[AuthGuard]
    },
    {
      path: 'art',
      component: ArtComponent,
      // canActivate:[AuthGuard]
    },
    {
      path: 'movie',
      component: MovieComponent,
      // canActivate:[AuthGuard]
    }

//{path: '**', component: PageNotFoundComponent}
  ]

export const routing = RouterModule.forRoot(APP_ROUTES)
