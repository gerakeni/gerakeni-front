import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Global} from "./global";
import {User} from "./user";

@Injectable()
export class ConnectionService {
  private user:User = null;

  constructor(private http: Http) {
  }

  public getData(dataUrl) {
    return this.http.get(dataUrl).map(res => res.json());
  }

  public postData(dataUrl,data){
    return this.http.post(dataUrl, JSON.stringify(data)).map(res => res.json());
  }

  public getUser(){
    return this.user;
  }

  public setUser(user){
    this.user = user;
  }

  public isUserConnected(){
    return this.user != null;
  }

}
