import {Component} from '@angular/core';
import {ConnectionService} from "../connection-service";
import {Global} from "../global";
import {User} from '../user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})
export class ConnectionComponent {
  private data;
  private dataUrl = '';
  private urlBasis = "http://localhost:8000/user";
  private connectionFail;

  constructor(private connectionService: ConnectionService, private router: Router) {
    this.data = {};
  }

  private setUrl() {
    if (Global.USER_SIGN) {
      this.dataUrl = this.urlBasis + '/subscribe';
    } else {
      this.dataUrl = this.urlBasis + '/authenticate';
    }
    return this.dataUrl;
  }

  private showComponent(){
    return Global.USER_SIGN;
  }

  formSubmit() {
    console.log(this.data);
    this.connectionService.postData(this.setUrl(), this.data).subscribe(
      data => {
        console.log(data);
        if (data.success && Global.USER_SIGN) {
          this.data ={};
          console.log('subscribe');
          Global.USER_SIGN = false;
        } else if (data.success) {
          this.connectionService.setUser(new User(this.data));
          this.router.navigateByUrl("/home").then((status) => {
            console.log('status : ' + status);
          }).catch((exception) => {
            console.error('Error', exception);
          });
        } else {
          this.connectionFail = true;
          console.log(this.connectionFail);
        }
      },
      error => console.log(error),
      () => console.log('finished')
    );
  }

}
