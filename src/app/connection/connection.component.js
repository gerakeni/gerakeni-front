"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var global_1 = require("../global");
var user_1 = require('../user');
var ConnectionComponent = (function () {
    function ConnectionComponent(connectionService, router) {
        this.connectionService = connectionService;
        this.router = router;
        this.dataUrl = '';
        this.urlBasis = "http://localhost:8000/user";
        this.data = {};
    }
    ConnectionComponent.prototype.setUrl = function () {
        if (global_1.Global.USER_SIGN) {
            this.dataUrl = this.urlBasis + '/subscribe';
        }
        else {
            this.dataUrl = this.urlBasis + '/authenticate';
        }
        return this.dataUrl;
    };
    ConnectionComponent.prototype.showComponent = function () {
        return global_1.Global.USER_SIGN;
    };
    ConnectionComponent.prototype.formSubmit = function () {
        var _this = this;
        console.log(this.data);
        this.connectionService.postData(this.setUrl(), this.data).subscribe(function (data) {
            console.log(data);
            if (data.success && global_1.Global.USER_SIGN) {
                _this.data = {};
                console.log('subscribe');
                global_1.Global.USER_SIGN = false;
            }
            else if (data.success) {
                _this.connectionService.setUser(new user_1.User(_this.data));
                _this.router.navigateByUrl("/home").then(function (status) {
                    console.log('status : ' + status);
                }).catch(function (exception) {
                    console.error('Error', exception);
                });
            }
            else {
                _this.connectionFail = true;
                console.log(_this.connectionFail);
            }
        }, function (error) { return console.log(error); }, function () { return console.log('finished'); });
    };
    ConnectionComponent = __decorate([
        core_1.Component({
            selector: 'app-connection',
            templateUrl: './connection.component.html',
            styleUrls: ['./connection.component.css']
        })
    ], ConnectionComponent);
    return ConnectionComponent;
}());
exports.ConnectionComponent = ConnectionComponent;
