import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  redirectTo(pathId){
    console.log('pathID' + pathId);
    this.router.navigateByUrl(pathId).then((status) => {
      console.log('status : ' + status);
    }).catch((exception) => {
      console.error('Error', exception);
    });
  }

}
