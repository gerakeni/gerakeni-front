"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var AccountComponent = (function () {
    function AccountComponent(connectionService, router) {
        this.connectionService = connectionService;
        this.router = router;
    }
    AccountComponent.prototype.ngOnInit = function () {
    };
    AccountComponent.prototype.deleteUser = function () {
        var _this = this;
        this.connectionService.postData("http://localhost:8000/user/deleteUser", { username: this.connectionService.getUser().username }).subscribe(function (data) {
            if (data.success) {
                _this.connectionService.setUser(null);
                _this.router.navigateByUrl("/connection").then(function (status) {
                    console.log('status : ' + status);
                }).catch(function (exception) {
                    console.error('Error', exception);
                });
            }
            console.log(data);
        }, function (error) { return console.log(error); }, function () { return console.log('finished'); });
    };
    AccountComponent.prototype.changePassword = function () {
        this.show = true;
    };
    AccountComponent.prototype.accountSubmit = function () {
        var _this = this;
        this.connectionService.postData("http://localhost:8000/user/modifyPassword", { username: this.connectionService.getUser().username, mdp: this.password }).subscribe(function (data) {
            if (data.success) {
                _this.connectionService.getUser().password = _this.password;
                _this.router.navigateByUrl("/home").then(function (status) {
                    console.log('status : ' + status);
                }).catch(function (exception) {
                    console.error('Error', exception);
                });
            }
            console.log(data);
        }, function (error) { return console.log(error); }, function () { return console.log('finished'); });
    };
    AccountComponent = __decorate([
        core_1.Component({
            selector: 'app-account',
            templateUrl: './account.component.html',
            styleUrls: ['./account.component.css']
        })
    ], AccountComponent);
    return AccountComponent;
}());
exports.AccountComponent = AccountComponent;
