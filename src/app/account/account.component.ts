import {Component, OnInit} from '@angular/core';
import {ConnectionService} from '../connection-service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  private password: string;
  private show: boolean;

  constructor(private connectionService: ConnectionService, private router: Router) {

  }

  ngOnInit() {
  }

  deleteUser() {
    this.connectionService.postData("http://localhost:8000/user/deleteUser",
      {username: this.connectionService.getUser().username}).subscribe(
      data => {
        if (data.success) {
          this.connectionService.setUser(null);
          this.router.navigateByUrl("/connection").then((status) => {
            console.log('status : ' + status);
          }).catch((exception) => {
            console.error('Error', exception);
          });
        }
        console.log(data);
      },
      error => console.log(error),
      () => console.log('finished')
    );
  }

  changePassword() {
    this.show = true;
  }

  accountSubmit() {
    this.connectionService.postData("http://localhost:8000/user/modifyPassword",
      {username: this.connectionService.getUser().username, mdp: this.password}).subscribe(
      data => {
        if (data.success) {
          this.connectionService.getUser().password = this.password;
          this.router.navigateByUrl("/home").then((status) => {
            console.log('status : ' + status);
          }).catch((exception) => {
            console.error('Error', exception);
          });
        }
        console.log(data);
      },
      error => console.log(error),
      () => console.log('finished')
    );
  }
}
