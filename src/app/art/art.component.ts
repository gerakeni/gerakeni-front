import { Component, OnInit } from '@angular/core';
import {ConnectionService} from "../connection-service";

@Component({
  selector: 'app-art',
  templateUrl: './art.component.html',
  styleUrls: ['./art.component.css']
})
export class ArtComponent implements OnInit {
  private search:string;
  private url = "http://localhost:8000/art/search";
  public response = {};
  constructor(private connectionService : ConnectionService) { }

  ngOnInit() {
  }
  searchArt(){
    console.log('search');
    if(this.search != ""){
      this.connectionService.postData(this.url,{search: this.search}).subscribe(
        data => {
          this.response = JSON.parse(data);
          console.log(this.response);
        },
        error => console.log(error),
        () => console.log('finished')
      );
    }
    else {
      console.log('criteria empty');
    }
  }

}
