import { Component, OnInit } from '@angular/core';
import {ConnectionService} from "../connection-service";

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.css']
})
export class MusicComponent implements OnInit {
  private search:string;
  private url = "http://localhost:8000/music/search";
  public response = {};
  constructor(private connectionService : ConnectionService) { }

  ngOnInit() {
  }
  searchMusic(){
    console.log('search');
    if(this.search != ""){
      this.connectionService.postData(this.url,{search: this.search}).subscribe(
        data => {
          this.response = JSON.parse(data);
          console.log(this.response);
        },
        error => console.log(error),
        () => console.log('finished')
      );
    }
    else {
      console.log('criteria empty');
    }
  }
}
